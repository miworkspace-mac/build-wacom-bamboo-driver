#!/bin/bash -ex

# CONFIG
prefix="WacomBambooDriver"
suffix=""
munki_package_name="WacomBambooDriver"
display_name="Wacom Bamboo Pen Tablet Driver"
category="System Morsel"
description="Wacom Pen Tablet Driver supports all Bamboo tablets, the Graphire4, Graphire Bluetooth & Graphire3 pen tablets, and USB-connected Cintiq 17SX, DTF-720, DTU-710, DTF-521 & DTF-510 pen displays."
url=`./finder.rb`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $NF } '`
echo Mounted on $mountpoint
pwd=`pwd`
pkg=`ls ${mountpoint}/*.pkg`
pkgutil --expand "${pkg}" "${pwd}"/build-root
(cd build-root; pax -rz -f "content.pkg/Payload")
hdiutil detach "${mountpoint}"

# Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 5 | sed 's/ /\\\\ /g; s/^/-f /'  | paste -s -d ' ' -`

# Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} | /bin/bash > app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "${pwd}/build-root/Applications/Pen Tablet.localized/Pen Tablet Utility.app/Contents/Info.plist" | sed 's/v//'`
#version=`defaults read "${plist}" version`
rm -rf build-root
# Remove "build-root" from file paths
perl -p -i -e 's/build-root//; s/.localized//' app.plist

#Remove Receipts from plist
/usr/libexec/PlistBuddy -c "Delete receipts" app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" category -string "${category}"
defaults write "${plist}" RestartAction "RequireLogout"

# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
